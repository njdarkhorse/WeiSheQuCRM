/**
 * Created by anti.
 */
var BACK_END_API = {};

switch (process.env.NODE_ENV) {
  case 'development':
    // 测试主机域名
    BACK_END_API.ROOT = 'https://api.hkfsvip.com';
    // 产品部分api地址
    BACK_END_API.HK = 'https://api.hkfsvip.com/api-hks/v1';
    // 官网部分api地址
    BACK_END_API.WEB = 'https://api.hkfsvip.com/api-web/v1';
    BACK_END_API.ACTIVE = 'https://api.hkfsvip.com/api-active/v1';
    break;
  case 'test':
    // 测试主机域名
    BACK_END_API.ROOT = 'https://api.hkfsvip.com';
    // 产品部分api地址
    BACK_END_API.HK = 'https://api.hkfsvip.com/api-hks/v1';
    // 官网部分api地址
    BACK_END_API.WEB = 'https://api.hkfsvip.com/api-web/v1';
    BACK_END_API.ACTIVE = 'https://api.hkfsvip.com/api-active/v1';
    break;
  case 'production':
    // 生产主机域名
    BACK_END_API.ROOT = 'https://xapi.hkfsvip.com';
    // 产品部分api地址
    BACK_END_API.HK = 'https://xapi.hkfsvip.com/api-hks/v1';
    // 官网部分api地址
    BACK_END_API.WEB = 'https://xapi.hkfsvip.com/api-web/v1';
    BACK_END_API.ACTIVE = 'https://xapi.hkfsvip.com/api-active/v1';
    break;
}

module.exports = BACK_END_API;
