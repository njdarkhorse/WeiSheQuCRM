import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    state: {
        path: 'https://dev.mc.nihome.cn/',
        storeId: '',
        userName: '',
        couponType: '',
        couponTypeId: '',
        couponData: '',
        showMask: false,
        editAdvItem: {},
        carDetailId: "", //卡券ID
        // photo_path: 'https://dev.nihome.cn/dev-v2/uploadFile/fileUpload?',
        // photo_path: 'https://dev.nihome.cn/uploadFile/fileUpload?',
        photo_path: 'https://dev.mc.nihome.cn/uploadFile/fileUpload?',
        base64_path: 'https://dev.mc.nihome.cn/uploadFile/fileUploadStr2',
        wxPhotoPath: 'https://dev.nihome.cn/uploadFile/uploadWxCert',
        nodePath: 'https://dev.mc.nihome.cn/ns/',
    },
    mutations: {

    },
    getters: { //属性就可以

    },
    actions: {

    },
})