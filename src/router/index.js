import Vue from 'vue'
import Router from 'vue-router'
// import Index from '@/components/index'
import BrowseDetail from '@/components/cardCoupons/browseDetail'
import CardCoupons from '@/components/cardCoupons/cardCoupons'
import CardCouponsList from '@/components/cardCoupons/cardCouponsList'
import CardCouponsDetail from '@/components/cardCoupons/cardCouponsDetail'
import Login from '@/components/setting/login'
import Record from '@/components/cardCoupons/record'

import StorePromotionList from '@/components/storePromotion/storePromotionList'
import StoreList from '@/components/storeManagement/storeList'
import RecommendStoreList from '@/components/storeManagement/recommendStoreList'
import AdvertList from '@/components/advertisement/advertList'
import Add_Advert from '@/components/advertisement/add_Advert'
import Edit_Advert from '@/components/advertisement/edit_Advert'
import OrderList from '@/components/orders/orderList'
import OrderDetails from '@/components/orders/orderDetails'
import BlanceList from '@/components/balance/blanceList'
import BlanceDetail from '@/components/balance/blanceDetail'
import BalanceWater from '@/components/balance/balanceWater'
import ActionList from '@/components/marketing/actionList'
import CashSet from '@/components/marketing/cashSet'
import AddAction from '@/components/marketing/addAction'

Vue.use(Router)

let router = new Router({
  base: '/app/',
  // mode: 'history',
  routes: [
    {
      path: '/browseDetail',
      name: 'BrowseDetail',
      component: BrowseDetail,
      meta: { showframe: true }
    },
    {
      path: '/record',
      name: 'Record',
      component: Record,
      meta: { showframe: true }
    },
    {
      path: '/cardCoupons',
      name: 'CardCoupons',
      component: CardCoupons,
      meta: { showframe: true }
    },
    {
      path: '/cashSet',
      name: 'CashSet',
      component: CashSet,
      meta: { showframe: true }
    },
    {
      path: '/actionList',
      name: 'ActionList',
      component: ActionList,
      meta: { showframe: true }
    },
    {
      path: '/addAction',
      name: 'AddAction',
      component: AddAction,
      meta: { showframe: true }
    },
    {
      path: '/cardCouponsList',
      name: 'CardCouponsList',
      component: CardCouponsList,
      meta: { showframe: true }
    },
    {
      path: '/cardCouponsDetail/:cardId',
      name: 'cardCouponsDetail',
      component: CardCouponsDetail,
      meta: { showframe: true }
    },
    {
      path: '/storePromotionList',
      name: 'storePromotionList',
      component: StorePromotionList,
      meta: { showframe: true }
    },
    {
      path: '/storeList',
      name: 'storeList',
      component: StoreList,
      meta: { showframe: true }
    },
    {
      path: '/balanceList',
      name: 'BlanceList',
      component: BlanceList,
      meta: { showframe: true }
    },
    {
      path: '/balanceDetail/:recordId',
      name: 'BlanceDetail',
      component: BlanceDetail,
      meta: { showframe: true }
    },
    {
      path: '/balanceWater',
      name: 'BalanceWater',
      component: BalanceWater,
      meta: { showframe: true }
    },
    {
      path: '/orderDetails/:orderId',
      name: 'OrderDetails',
      component: OrderDetails,
      meta: { showframe: true }
    },
    {
      path: '/orderList',
      name: 'OrderList',
      component: OrderList,
      meta: { showframe: true }
    },
    {
      path: '/recommendStoreList',
      name: 'RecommendStoreList',
      component: RecommendStoreList,
      meta: { showframe: true }
    },
    {
      path: '/advertList',
      name: 'advertList',
      component: AdvertList,
      meta: { showframe: true }
    },
    {
      path: '/add_Advert',
      name: 'add_Advert',
      component: Add_Advert,
      meta: { showframe: true }
    },
    {
      path: '/edit_Advert',
      name: 'edit_Advert',
      component: Edit_Advert,
      meta: { showframe: true }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: { showframe: false }
    },
    {
      path: '/',
      name: 'Login',
      component: Login,
      meta: { showframe: false }
    },
  ]
})
router.beforeEach((to, from, next) => {
  if (to.matched.length === 0) { //如果未匹配到路由
    from.name ? next({ name: from.name }) : next('/'); //如果上级也未匹配到路由则跳转登录页面，如果上级能匹配到则转上级路由
  } else {
    next(); //如果匹配到正确跳转测试
  }
});
export default router;
