// import 'babel-polyfill'
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
// import 'vue-mui/src/style.css'
import VueResource from 'vue-resource'
import store from './store/user'
import '../static/font/iconfont.css'
import '../static/base.css'

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(VueResource)
Vue.filter('Number', function (Number) {
  return Number.toFixed(2)
})
Vue.filter('TimeD', function (format) {//时间精确到秒
  if ((format + "").length == 10) format = Number(format) * 1000
  function add0(m) {
    return m < 10 ? '0' + m : m
  }
  //shijianchuo是整数，否则要parseInt转换
  var time = new Date(format);
  var y = time.getFullYear();
  var m = time.getMonth() + 1;
  var d = time.getDate();
  var h = time.getHours();
  var mm = time.getMinutes();
  var s = time.getSeconds();

  return y + '-' + add0(m) + '-' + add0(d) + ' ' + add0(h) + ':' + add0(mm)
})
Vue.filter('TimeE', function (format) {//时间精确到秒
  let arr = []
  let str = ``
  arr=format.split('T')
  str=format.split('T')[0]+' '+arr[1].split('.')[0]
  console.log(str)
  return str
})
Vue.filter('Time', function (format) {//时间精确到秒
  if ((format + "").length == 10) format = Number(format) * 1000
  function add0(m) {
    return m < 10 ? '0' + m : m
  }
  //shijianchuo是整数，否则要parseInt转换
  var time = new Date(parseInt(format));
  var y = time.getFullYear();
  var m = time.getMonth() + 1;
  var d = time.getDate();
  var h = time.getHours();
  var mm = time.getMinutes();
  var s = time.getSeconds();

  return y + '-' + add0(m) + '-' + add0(d) + ' ' + add0(h) + ':' + add0(mm)
})
Vue.filter('Time1', function (format) {//日期
  if ((format + "").length == 10) format = Number(format) * 1000
  function add0(m) {
    return m < 10 ? '0' + m : m
  }

  //shijianchuo是整数，否则要parseInt转换
  var time = new Date(parseInt(format));
  var y = time.getFullYear();
  var m = time.getMonth() + 1;
  var d = time.getDate();
  var h = time.getHours();
  var mm = time.getMinutes();
  var s = time.getSeconds();
  return y + '-' + add0(m) + '-' + add0(d)
})
Vue.prototype.Time = (format) => {
  // format = new Date(format)
  if ((format + "").length == 10) format = Number(format) * 1000
  if (1) {
    function add0(m) {
      return m < 10 ? '0' + m : m
    }
    //shijianchuo是整数，否则要parseInt转换
    var time = new Date(parseInt(format));
    var y = time.getFullYear();
    var m = time.getMonth() + 1;
    var d = time.getDate();
    var h = time.getHours();
    var mm = time.getMinutes();
    var s = time.getSeconds();
    return y + '-' + add0(m) + '-' + add0(d) + ' ' + add0(h) + ':' + add0(mm)
  }
}
Vue.prototype.Time1 = (format) => {
  if ((format + "").length == 10) format = Number(format) * 1000
  if (1) {
    function add0(m) {
      return m < 10 ? '0' + m : m
    }

    //shijianchuo是整数，否则要parseInt转换
    var time = new Date(parseInt(format));
    var y = time.getFullYear();
    var m = time.getMonth() + 1;
    var d = time.getDate();
    var h = time.getHours();
    var mm = time.getMinutes();
    var s = time.getSeconds();
    return y + '-' + add0(m) + '-' + add0(d)
  }
}
Vue.prototype.coverTimer = (str) => {
  // var str = 'Mon May 16 2016 18:48:34 GMT+0800 (中国标准时间)';
  const time = str.replace(/GMT.+$/, '')// Or str = str.substring(0, 24)
  const d = new Date(time)
  const a = [d.getFullYear(), d.getMonth() + 1, d.getDate()]
  for (let i = 0, len = a.length; i < len; i++) {
    if (a[i] < 10) {
      a[i] = '0' + a[i]
    }
  }
  str = a[0] + '-' + a[1] + '-' + a[2]
  return str
}

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {App}
})
