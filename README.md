# nj-h5app  
[toc]

## 概述 

点餐小程序转H5

## 环境搭建 

开发环境：nodejs v4.5+    
前端技术：html5, VUE v2.0+   

## 安装运行

- 安装nodejs/npm，自行搜索
- 安装VUE 
- 执行`npm run dev`运行

## 部署