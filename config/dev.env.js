var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BACK_END_API: {
    // 测试主机域名
    ROOT: 'https://api.hkfsvip.com',
    HK: 'https://api.hkfsvip.com/api-hks/v1',
    WEB: 'https://api.hkfsvip.com/api-web/v1',
    ACTIVE: 'https://api.hkfsvip.com/api-active/v1',
    NWEB:'http://120.76.77.230:8888'
  }
})
