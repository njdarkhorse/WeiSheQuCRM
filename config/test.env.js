var merge = require('webpack-merge')
var devEnv = require('./dev.env')

module.exports = merge(devEnv, {
  NODE_ENV: '"testing"',
  BACK_END_API: {
    // 测试主机域名
    ROOT: 'https://api.hkfsvip.com',
    HK: 'https://api.hkfsvip.com/api-hks/v1',
    WEB: 'https://api.hkfsvip.com/api-web/v1',
    ACTIVE: 'https://api.hkfsvip.com/api-active/v1'
  }
})
