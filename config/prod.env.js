module.exports = {
  NODE_ENV: '"production"',
  BACK_END_API: {
    // 主机域名
    ROOT: 'https://xapi.hkfsvip.com',
    HK: 'https://xapi.hkfsvip.com/api-hks/v1',
    WEB: 'https://xapi.hkfsvip.com/api-web/v1',
    ACTIVE: 'https://xapi.hkfsvip.com/api-active/v1'
  }
}
